# Zenity Stamp

About
-----

Simple Script that will call up Zenity windows in Ubuntu 18.04+ (but it should probably work as far back as 16.04) in order to stamp PDF's with pre-prepared stamps using pdftk, presenting the user with various options.

Theoretically, this script should work on any Linux system that has both zenity and pdftk installed, although this has not been tested.

Requirements
------------

This script requires the user to have installed both `zenity` and `pdftk`, without which it will not work. It attempts to notify the user if either one is not installed.