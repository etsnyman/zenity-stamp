#!/bin/bash

# IMPORTANT NOTE:
# This script requires that both zenity and pdftk are installed, and are available through the commands "zenity" and "pdftk" respectively.

###################################################
#                                                 #
# 1. Firstly, all the functions used are defined: #
#                                                 #
###################################################

# -------------------------------------------------------------------------------------- #
# Lets the user know if something has gone wrong and deletes the temporary file created: #
# -------------------------------------------------------------------------------------- #

error_report () {

	rm $tempFileName
	echo "This program will now exit."
	zenity --error --title="" --width=300 --text="This program will now exit." --ok-label="Exit"
	exit 1
}

# ------------------------------------------- #
# Deletes the temporary file created on exit: #
# ------------------------------------------- #

cleanup () {

	rm $tempFileName
}

# ------------------------------------------------------------------------------------------------- #
# Simple check whether both zenity and pdftk are installed. If not, it attempts to notify the user: #
# ------------------------------------------------------------------------------------------------- #

startup_test () {

	if ! [ -x "$(command -v zenity)" ]; then
		notify-send "Error: Zenity is not installed." >&2
		echo "Error: Zenity is not installed."
		exit 1
	fi

	if ! [ -x "$(command -v pdftk)" ]; then
		notify-send "Error: PdfTk is not installed." >&2
		echo "Error: PdfTk is not installed."
		exit 1
	fi
}

# ---------------------------------------------------------------------- #
# Just a simple dialog box to start, or exit if the person presses quit: #
# ---------------------------------------------------------------------- #

startup_question () {

	zenity --question --width=400 --title="PDF File Stamper" --text "Please first choose the file(s) you wish to have stamped." --ok-label="Choose file(s)" --cancel-label="Quit"

	if [ $? -eq 1 ]; then
		exit 1
	fi
}

# -------------------------------------------------------------------------------------------------------------- #
# Choose all file(s) to be stamped, writes them to array $listfilenames, and exits if the person presses cancel: #
# -------------------------------------------------------------------------------------------------------------- #

input_files () {
	i=0
	while [ $i -eq 0 ]; do

		inputFileNames=$(zenity --file-selection --multiple --title="Select original PDF documents to stamp" --file-filter='*.pdf')

		if [ $? -eq 1 ]; then # Exits entire program if Cancel is pressed.
			exit 1
		fi

		IFS='|' read -ra listFileNames <<< "$inputFileNames"

		# Lists the files for you to check if they are correct, otherwise it goes back:
		zenity --list --width=750 --height=350 --title="Original documents" --text="Please make sure that you have chosen the correct documents." --column="Current list of files:" "${listFileNames[@]}" --cancel-label="Go back" --ok-label="Continue"

		if [ $? -eq 0 ]; then # Only the "OK" button gives feedback
			i=1 # If the files are correct, then the while loop will be done.
		fi
	done
}

# ------------------------------------------------- #
# Choose the PDF file that will serve as the stamp: #
# ------------------------------------------------- #

input_stamp () {

	inputStamp=$(zenity --list --radiolist --width=350 --height=250 --title="Choose stamp" --text="Which PDF file would you like to use as a stamp?" --hide-column=2 --print-column=2 --column="Select"  --column="id" --column="Option" TRUE 0 "Choose your own stamp" FALSE 1 "Use stamp.pdf in current directory" --cancel-label="Quit")

	if [ $? -eq 1 ]; then # Exits entire program if Cancel is pressed.
		echo "Exiting at input_stamp"
		exit 1
	fi

	if [ $inputStamp -eq 0 ]; then # If first option is chosen, select a file to stamp and set it as $stampName.
		stampName=$(zenity --file-selection --multiple --title="Select stamp" --file-filter='*.pdf')

		if [ $? -eq 1 ]; then # Exits entire program if Cancel is pressed.
			echo "Exiting at input_stamp"
			exit 1
		fi	
	elif [ $inputStamp -eq 1 ]; then # If second option is chosen, select stamp.pdf in current directory.

		if [[ -n $(find $(pwd) -name 'stamp.pdf') ]]; then # Check to see if stamp.pdf exists.
			stampName="$(pwd)/stamp.pdf"
		else # If it doesn't exist, make user choose a new stamp file.
			zenity --question --width=300 --title="Not found" --text="There is no stamp.pdf in your current directory. Please choose a different file instead." --cancel-label="Quit" --ok-label="Choose" 
				if [ $? -eq 1 ]; then
					exit 1
				fi
			stampName=$(zenity --file-selection --multiple --title="Select stamp" --file-filter='*.pdf')
		fi
	fi
}

# ----------------------------------------------------------------------------- #
# Choose exactly how the original document(s) will be stamped by the stamp PDF: #
# ----------------------------------------------------------------------------- #

stamp_type_chooser () { 

	inputStampType=$(\

	zenity --list \
		--radiolist \
		--width=950 \
		--height=200 \
		--title="Stamp type" \
		--text="Choose how you would like to stamp:" \
		--hide-column=2 \
		--print-column=2 \
		--column="Select"  \
		--column="id" \
		--column="Option" \
		--column="Explanation" \
		TRUE 0 "Extract and stamp first page" "The first page of the original document(s) will be extracted and stamped." \
		FALSE 1 "Stamp all pages" "All pages of the original document(s) will be stamped with only the first page of the stamp file." \
		FALSE 2 "Multi-Stamp" "All pages of the original documents(s) will be stamped with all available pages of the stamp file."\
		 --cancel-label="Quit")
	# 0 = extract; 1 = single stamp all pages; 2 = multistamp multiple pages
}

ready_to_go () {

	# First check that everything we need is there!
	if [ -z "$inputStampType" ] || [ -z "$stampName" ] || [ -z "$listFileNames" ]; then
		zenity --error --title="Error" --width=300 --text="An error has occurred. This program will now exit." --ok-label="Exit"
		echo "An error has occurred. This program will now exit."
		exit 1
	fi

	# Now ask if the user wants to continue.


	inputDirectory=$(zenity --list --radiolist --width=350 --height=250 --title="Choose output directory" --text="Which directiry would you like to output the final stamped documents(s)?" --hide-column=2 --print-column=2 --column="Select"  --column="id" --column="Option" TRUE 0 "Choose a directory" FALSE 1 "Use current directory" --cancel-label="Quit")

	if [ $inputDirectory -eq 0 ]; then # If first option is chosen, select a directory and set it to $outputDirectory.

		outputDirectory=$(zenity --file-selection --title="Choose output directory" --directory)

		if [ $? -eq 1 ]; then # Exits entire program if Cancel is pressed.
			exit 1
		fi

	elif [ $input_input_stamp -eq 1 ]; then # If second option is chosen, select current directory.

		outputDirectory=$(pwd)
	fi

	zenity --question --width=500 --title="Continue" --text "If you are confident you have chosen the correct files, and you have chosen all the correct options, please press Continue." --ok-label="Continue" --cancel-label="Quit"

	if [ $? -eq 1 ]; then
		exit 1
	fi

	# Things that need to be done before pdftk does its magic:
#'s/ /\\ /\(\[\|\]\)//g'
	newOutputDirectory=$(echo "$outputDirectory" | sed 's/ /\\ /g' | sed -e 's/(/\\(/g' | sed -e 's/)/\\)/g' | sed -e 's/\[/\\[/g' | sed -e 's/\]/\\]/g') # Escapes the white spaces in output directory.
	total=${#listFileNames[@]}	 # Find number of filenames in total.
	percentage=$((1*10000/total))	 # Determine percentage of each separate file without decimal points. Needs to be divided by 100.
	digits="${#total}"	 # Find number of digits in total number.
	leading="%0${digits}d"	 # Create number of leading zero's.
	randomNew=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 4 | head -n 1)	 # Generates a random string so that old output files are not overwritten by accident.
	tempFileName="temp_"$randomNew".pdf"	 # Create temporary file name using random string.
	tempTextFile="tempfile_"$randomNew".txt"	 # Create temporary workaround text file with the output file names.
	tempTextFile2="tempfile_"$randomNew"_(2).txt"	 # Create another temporary workaround text file with the output file names.

}

stamp_extract () {

	j=0 # Set both to zero.
	completePercent=0

	( # This is used with Zenity. The completePercent percentage is sent to Zenity.
	for fileName in "${listFileNames[@]}"; do # Loop through the array of file names.

		entryNumber=$(printf $leading $j ) # Start numbering entries, starting at 0 with correct number of leading zero's.
		outputFileName=$newOutputDirectory"/output_"$randomNew"_"$entryNumber".pdf" # Make output file name the correct name and directory.

		pdftk A="$fileName" cat A1 output $tempFileName # Extract first page of input file into a temporary file.
		pdftkStamp="pdftk \""$tempFileName"\" stamp \""$stampName"\" output "$outputFileName # Create the stamp command that does not work on its own. 	#pdftk $tempFileName stamp $stampName output $outputFileName # THIS DOES NOT WORK!
		eval $pdftkStamp # Excecute the stamp command to output file.
		rm $tempFileName # Remove temporary file.

		j=$((j+1)) # Increment file number.
		echo $outputFileName >> $tempTextFile

		echo $(( completePercent / 100 )) # Send percentage to zenity. Divided by 100 finally.
		completePercent=$((percentage+completePercent)) # Increment Percentage.

	done
	)|

	zenity --progress --auto-close --auto-kill --title="Progress" --text="Stamping documents(s)..." --percentage=0
}

stamp_all () {

	j=0 # Set both to zero.
	completePercent=0

	( # This is used with Zenity. The completePercent percentage is sent to Zenity.
	for fileName in "${listFileNames[@]}"; do # Loop through the array of file names.

		entryNumber=$(printf $leading $j ) # Start numbering entries, starting at 0 with correct number of leading zero's.
		outputFileName=$newOutputDirectory"/output_"$randomNew"_"$entryNumber".pdf" # Make output file name the correct name and directory.
		##newFileName=$(echo "$fileName" | sed 's/ /\\ /g') # Escapes the white spaces in output directory.

		pdftkStamp="pdftk \""$fileName"\" stamp \""$stampName"\" output "$outputFileName # Create the stamp command that does not work on its own. 	#pdftk $tempFileName stamp $stampName output $outputFileName # THIS DOES NOT WORK!
		eval $pdftkStamp # Excecute the stamp command to output file.

		j=$((j+1)) # Increment file number.
		echo $outputFileName >> $tempTextFile

		echo $(( completePercent / 100 )) # Send percentage to zenity. Divided by 100 finally.
		completePercent=$((percentage+completePercent)) # Increment Percentage.

	done
	)|

	zenity --progress --auto-close --auto-kill --title="Progress" --text="Stamping documents(s)..." --percentage=0
}

stamp_multistamp () {

	j=0 # Set both to zero.
	completePercent=0

	( # This is used with Zenity. The completePercent percentage is sent to Zenity.
	for fileName in "${listFileNames[@]}"; do # Loop through the array of file names.

		entryNumber=$(printf $leading $j ) # Start numbering entries, starting at 0 with correct number of leading zero's.
		outputFileName=$newOutputDirectory"/output_"$randomNew"_"$entryNumber".pdf" # Make output file name the correct name and directory.
		##newFileName=$(echo "$fileName" | sed 's/ /\\ /g') # Escapes the white spaces in output directory.

		pdftkStamp="pdftk \""$fileName"\" multistamp \""$stampName"\" output "$outputFileName # Create the stamp command that does not work on its own. 	#pdftk $tempFileName stamp $stampName output $outputFileName # THIS DOES NOT WORK!
		eval $pdftkStamp # Excecute the stamp command to output file.

		j=$((j+1)) # Increment file number.
		echo $outputFileName >> $tempTextFile

		echo $(( completePercent / 100 )) # Send percentage to zenity. Divided by 100 finally.
		completePercent=$((percentage+completePercent)) # Increment Percentage.

	done
	)|

	zenity --progress --auto-close --auto-kill --title="Progress" --text="Stamping documents(s)..." --percentage=0
}

output_files () {

	tempTextFile2="tempfile_"$randomNew"_(2).txt"
	sed 's/\\//g' $tempTextFile > $tempTextFile2

	while IFS= read -r line; do
	   outputList+=("$line")
	done < $tempTextFile2

	rm $tempTextFile $tempTextFile2

	zenity --list --width=750 --height=350 --title="Result" --text="The following output files have been successfully created:" --column="Stamped output files" "${outputList[@]}"
}

#################################################################
#                                                               #
# 2. Here, below, all the functions are actully called and run: #
#                                                               #
#################################################################

trap 'error_report' ERR # User is notified if something went wrong.
trap 'cleanup' EXIT # Delete temporary file temp0987654321abcZENITY.pdf if something goes wrong.

startup_test # Simple check whether both zenity and pdftk are installed.

startup_question # Just a simple dialog box to start the process.

input_files # Choose all file(s) to be stamped.

input_stamp # Choose the PDF file that will serve as the stamp

stamp_type_chooser # Chooses the type of stamping.

ready_to_go # Does the last checks and assigns all the variables.

echo "Stampy type: " $inputStampType

if [ $inputStampType -eq 0 ]; then	 # Choose the correct type of stamping to do, and call the necessary function.	# 0 = extract; 1 = single stamp all pages; 2 = multistamp multiple pages

	stamp_extract

elif [ $inputStampType -eq 1 ]; then

	stamp_all

elif [ $inputStampType -eq 2 ]; then

	stamp_multistamp

else
	echo "An error has occurred. This program will now exit."
	zenity --error --title="Error" --width=300 --text="An error has occurred. This program will now exit." --ok-label="Exit"
	exit 1
fi

output_files
